# SSC0511-Organizacao-de-Computadores-Digitais
SSC0511 - Organização de Computadores Digitais

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@@icmc.usp.br
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2021 - Segundo semestre
- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas - https://docs.google.com/spreadsheets/d/1LlNnokYW4I-M-51BgPAiQenBfsjsFKRFwpROH4TmLOg/edit?usp=sharing
- Você pode acompanhar sua Frequência aqui: https://docs.google.com/spreadsheets/d/1d9DvYIbG7a2Lv-u7pGivsbXZwLXh97Vox9ndHbi64Wc/edit?usp=sharing
- As aulas começam dia 20/08/2021 e serão transmitidas pelo google Meet (sempre com o código https://meet.google.com/dau-zjhn-cvy) no horário normal da disciplina: Terça 21:00h-22:40h e Sexta 19:00h-20:40h
- As aulas serâo gravadas e disponibilisadas nessa plataforma
- Essa disciplina irá utilizar o projeto do Processador ICMC, que é um processador RISC de 16 bits implementado em FPGA - todas as ferramentas de software e hardware estão disponíveis no Github do projeto: https://github.com/simoesusp/Processador-ICMC
- Os alunos serão divididos em grupos de 3-4 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!


## Apresentação dos Trabalhos: Insira as informações do seu grupo no arquivo do GoogleDocs:
https://docs.google.com/spreadsheets/d/1fxI3oqT7LWC4oNkqqYeARpJeLeFvLgMfFyPZtmN1sP8/edit?usp=sharing



## Processador ICMC - https://github.com/simoesusp/Processador-ICMC

- Nossa disciplina irá usar um Processador desenvolvido pelos próprios alunos do ICMC disponível neste repositório do github

## Simulador para programação em Assembly 
- Nossa disciplina irá usar um simulador para desenvolver programas em linguagem Assembly que poderá ser encontrado para Windows, Linux e MacOS em: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/
- Para Windows, fiz um link super fácil de instalar: https://github.com/simoesusp/Processador-ICMC/blob/master/Install_Packages/Simulador_Windows_Tudo_Pronto_F%C3%A1cil%20(1).zip
  - Esse zip já vem inclusive com o sublime configurado para escrever o software (incluindo a sintaxe highlight) e o montador e o simulador já configurado para ser chamado com a tecla F7
  - Para instalar basta fazer o download na area de trabalho ou na pasta Documentos
  - Entrar na pasta ..\Simulador\Sublime Text 3
  - Executar o sublime: "sublime_text.exe"
  - Se ele pedir, NÃ0 FAÇA O UPDATE !!!!!!!!!!!!!!!
  - Vá em File - Open File e volte uma pasta para ..\Simulador\
  - Abra o sw em Assembly chamado Hello4.ASM
  - Teste se está tudo funcionando chamando o MONTADOR e o SIMULADOR com a tecla F7
  - Apartir daí pode-se salvar o sw com outro nome e fazer novos programas
  - Apenas preste atenção para estar na pasta ..\Simulador\
  - Se der o erro: [Decode error - output not utf-8] é porque você não está na pasta ..\Simulador\
  - ... Ou basta mudar o formato para utf-8 e salvar...


## Avaliação
- A avaliação será por meio de uma PROVA e da apresentação de um trabalho - Implementação de um Jogo em Linguagem Montadora (assembly) no processador ICMC (https://github.com/simoesusp/Processador-ICMC/tree/master/Processor_FPGA) ou no seu simulador físico (https://github.com/simoesusp/Processador-ICMC/tree/master/Simple_Simulator)
- Os alunos serão divididos em grupos de 2-3 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação dos trabalhos Turma 2021 - Segundo semestre:
- Apresentação dos trabalhos pelo Google Meet será na SEMANA XX de Janeiro: marcar o dia escolhido para apresentacao na tabela do arquivo:  ==> LINK DOCS: https://docs.google.com/spreadsheets/d/1fxI3oqT7LWC4oNkqqYeARpJeLeFvLgMfFyPZtmN1sP8/edit?usp=sharing

  - Apresentação dos trabalhos pelo Google Meet será na SEMANA XX de Dezembro

  - Insira seu projeto e reserve um horario para apresentar no arquivo do GoogleDocs, informando: TÍTULO DO PROJETO - nomes dos alunos - NUMERO USP dos alunos - link pro github/gitlab

## Trabalho da Disciplina:
- 1) Implementar um Jogo em Assembly
- 2) Construir o Hardware do Processador no "Simple Simulator" ou em VHDL na FPGA
- 3) Propor uma modificação na Arquitetura do Procassador(uma nova Instrução, por exemplo) e implementá-la no "Simple Simulator" ou em VHDL na FPGA
     - Para isso, deve-se modificar o Código do Simulador (ou FPGA)
     - Modificar o Montador para que reconheça sua nova Instrução (exemplo - [Link](https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0119-Pratica-em-Organizacao-de-Computadores/Modificar_Montador/README.md))
     - Modificar o Read.me do seu Github/Gitlab, descrvendo sua nova Instrução (ou alteraçao na Arquitetura)
- 4) OPCIONAL: reescrever o código do Jogo para incorporar a modificação no Hardware e testar no "OpenGL_Simulator" ou na FPGA

### Documentação do Trabalho no Github/Gitlab
- Criar uma conta sua no Github/gitlab
- Os projetos devem conter um Readme explicando o projeto e o software deve estar muito bem comentado!!
- Incluir no seu Github/gitlab: o Código do Simulador do Processador modificado, o JOGO.ASM e o CPURAM.MIF, e caso tenha alterado, o CHARMAP.MIF. 
- Obrigatório: incluir um VÍDEO DE VOCË explicando o projeto (pode ser somente uma captura de tela...) - Upa o vídeo no youtube ou no drive e poe o link no Readme.
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE por google Meet: a ser informado o link




# Aulas a Distância 2021 - Segundo Semestre

## Processador ICMC
- Link para o projeto do nosso próprio Processador: https://github.com/simoesusp/Processador-ICMC

## Simulador em C
- Link para o código fonte do Simple Simulator: https://github.com/simoesusp/Processador-ICMC/tree/master/Simple_Simulator

## Aula 01
- Link para a Aula 01 (17/08/20) - sempre com o código https://meet.google.com/dau-zjhn-cvy
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia






# Aulas a Distância 2020 - Segundo Semestre

## Aula 01
- Link para a Aula 01 (25/08/20) - https://drive.google.com/file/d/1uCjgG3x-rvUUT5WCgmM30Vp3nn1tdXnS/view?usp=sharing
- Material (Fotos da Lousa, PDFs...) - https://gitlab.com/simoesusp/disciplinas/-/tree/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia

## Aula 02
- Link para a Aula 02 (28/08/20) - https://drive.google.com/file/d/1XhVfbnCKVMJdJqpYyDw98uj1SYDWMY6r/view?usp=sharing
- **kkrieger** - Jogo de 96Kb - https://www.youtube.com/watch?v=mqHwJSsmCBc

## Aula 03
- Link para a Aula 03 (01/09/20)- https://drive.google.com/file/d/1JwWmGyU9UhLFOW20--tS7y2Ob_Q5xPQR/view?usp=sharing

## Aula 04
- Link para a Aula 04 (04/09/20)- https://drive.google.com/file/d/1NNxFfmblwA3Fibjx37OsjRrruYUihx43/view?usp=sharing

## Aula 05
- Link para a Aula 05 (08/09/20)- https://drive.google.com/file/d/12dwvUuZ4tnsv_YEl-ai4mc1M7WSyHaVU/view?usp=sharing
- Nesta aula tivemos problemas técnicos com a Internet e tivemos que parar as 10:05h - Continuaremos a matéria deste ponto na aula 06
- Introdução à Organisação de Computadores - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/instrucoes_10_2016.pdf
- História da Computação - Máquina Analítica de Babbage - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0511-Organizacao-de-Computadores-Digitais/MaterialAulaDistancia/Hist%C3%B3ria_da_Computa%C3%A7%C3%A3o_n1.pdf

## Aula 06
- Link para a Aula 06 (11/09/20)- https://drive.google.com/file/d/1s2yUZkVC64b9D8OVcDfDjiBKdW4uJUyc/view?usp=sharing

## Aula 07
- Link para a Aula 07 (15/09/20)- https://drive.google.com/file/d/1kkj188bWtGr6bySVMMnUe7G4KvPG5N3k/view?usp=sharing

## Aula 08
- Link para a Aula 08 (18/09/20)- https://drive.google.com/file/d/1NnUeRJfa7QtYRtnyqiM5SVWYDK4R74Vl/view?usp=sharing

## Aula 09
- Link para a Aula (22/09/20)- https://drive.google.com/file/d/1LUtXgYydN3Wb1lU77fiG9wqo5MizgHkM/view?usp=sharing

## Aula 10
- Link para a Aula (25/09/20)- https://drive.google.com/file/d/1gG5MXr7dv2UYY3T1-pfjDIpWkY5qdbXS/view?usp=sharing

## Aula 11
- Link para a Aula (06/10/20)- https://drive.google.com/file/d/1Gr6bqKnHxyKa6D6Z9a4mpK4dqYRj_65C/view?usp=sharing

## Aula 12
- Link para a Aula (09/10/20)- https://drive.google.com/file/d/18SpWewIqld64xbNpNFmSMlH3qIVKQPZk/view?usp=sharing

## Aula 13
- Link para a Aula (13/10/20) - https://drive.google.com/file/d/1D9-WEf5rRwcllkNGtB64w-xO8m6FS0yJ/view?usp=sharing

## Aula 14
- Link para a Aula (16/10/20)- https://drive.google.com/file/d/1HWH7ZQ0LzTQbz25Ae2WOdqipvcB_oqFN/view?usp=sharing
- Discussão Sobre ELD - https://drive.google.com/file/d/1UM3TfuutZlm2-OjH6bP9DHiXYhFzYGmW/view?usp=sharing

## Aula 15
- Link para a Aula (20/10/20)- https://drive.google.com/file/d/1nb3orHQ7w7k8qAT_pPKj5VDOOoSkyqsc/view?usp=sharing

## Aula 16
- Link para a Aula (23/10/20) - https://drive.google.com/file/d/1FzFhQpoO1uSsq6lwoeSE82GlSdDdq9Eq/view?usp=sharing

## Aula 17
- Link para a Aula (27/10/20) - https://drive.google.com/file/d/1kfp3fgmiW0fMC9zSIvTezZCKoN-763Ji/view?usp=sharing

## Aula 18
- Link para a Aula (06/11/20) - https://drive.google.com/file/d/1avGwfbqxiGsKWtDsiSjqRFcu-xVZ-Jg6/view?usp=sharing- 

## Aula 19
- Link para a Aula (10/11/20) - https://drive.google.com/file/d/1n0dtwUbpwadk9paAVEY_KItudTY5EEs2/view?usp=sharing

## Aula 20
- Link para a Aula (13/11/20) - https://drive.google.com/file/d/1oNd7cBQd3XvoL77biBhjkToKbWz_79-5/view?usp=sharing

## Aula 21
- Link para a Aula (17/11/20) - https://drive.google.com/file/d/1Lwq-EHmM8IADjTrIMjeCXG1UkaDwLoUd/view?usp=sharing

## Aula 22
- Link para a Aula (20/11/20) - https://drive.google.com/file/d/1I8gzp2xBDKvNozNEMBIvTSw9148cOBH3/view?usp=sharing

## Aula 23
- Link para a Aula (24/11/20) - https://drive.google.com/file/d/1IJDyGPvWEbK4dU3slQh02yU3WE4WNi7D/view?usp=sharing

## Aula 24
- Link para a Aula (27/11/20) - https://drive.google.com/file/d/1U5PeopuvSSIEQftq5r3VXd9sYvDJ4LJQ/view?usp=sharing 

## Aula 25
- Link para a Aula (01/12/20) - https://drive.google.com/file/d/1irxHuz4D7F0MczhifJSMlD8qTIQ5VCJz/view?usp=sharing

## Aula 26
- Link para a Aula (04/12/20) - https://drive.google.com/file/d/1O67Lfnr3SU5xGH1wCGrBuv4IlDmu6vqv/view?usp=sharing

## Aula 27
- Link para a Aula (08/12/20) - https://drive.google.com/file/d/1KvHkfm9ZSoTm6Bld6L59g4RQu67A0Vzi/view?usp=sharing

## Aula 28
- Link para a Aula (11/12/20) - https://drive.google.com/file/d/1viZ24GfK4MxDPJLNs1fV5iFmmmGMM2hd/view?usp=sharing

## Aula 29
- Link para a Aula (15/12/20) - https://drive.google.com/file/d/1R2nEMtIGd-bwAQXdeZDlDyQkK_VkkOmz/view?usp=sharing


## Lista de projetos 2019

- Arrows (Bruno Pagno, João mello, Lucas Saliba): https://github.com/bruno-pagno/JogoSetas
- Snake 3 fases aumentando nível (Henrique Francischeti Calil) https://gitlab.com/henriquefcalil/snake---org-comp/blob/master/snake.asm
- Key Bomb Membros : Gabriel Guimarães Vilas Boas Marin, Igor Guilherme Pereira Loredo, Felipe Sampaio Amorim, Link : https://github.com/Mapromu/Key-Bomb
- Verme do Tusca (Raphael Melloni Trombini, https://gitlab.com/raphaeltrombini/verme-do-tusca/tree/master
- Game_of_Life_autômato celular (William Carlos Giovanetti Gomes Link: https://github.com/biw7/Game_of_life-Assembly
- RPG Universitário (Lucas Braga, Sen Chai, Vinicius Carvalho) https://github.com/vgdcarvalho/lab_org/tree/master
- Quiz (Bruno Maximo, Álefe Silva, Pedro Chrisfofoletti) https://github.com/brunomaximom/quiz
- Quase golfe (Guilherme Filipe Feitosa dos Santos e Leila Gomes Ferreira https://github.com/leilagomesf/jogo_assembly
- ICMC WARS: Gabriel Cocenza, Ingrid Otani, Maria Vitória Inocencio - https://github.com/gabrielcocenza/ICMC_WARS
- Number Sequence: Marcio Guilherme Vieira Silva, Maurício José da Silva Filho, Rafael Garcia Fortunato - https://github.com/mjsilvafilho/org_comp
- Nova Velha: Daniel De Marco Fucci - https://github.com/danikyl/Velha_Nova/blob/master/velha.asm
- FlappyBird: Joao Vitor Salvini, Felipe Vinicius de Souza, Sara Dib, Joao Pedro Kosur - https://gitlab.com/jv_salvini/flappybird/blob/master/Flappy.asm



### Lista de projetos 2018
- BlackJack: Andrey Garcia, Gabriel Rodrigues Machado, Markel Berestov, Rafaela Souza Silva - https://github.com/GabrielMachado11/BlackJackAssembly
- Arrow: Gabriel Muniz Morao, Juliana Yendo, Stella Granatto Justo - Repositorio: https://github.com/stellajusto/arrow-assembly | Youtube: https://www.youtube.com/watch?v=qqL6hVRvEkc
- Ultra Alien Fighter: Juliana de Mello Crivelli - https://github.com/jumc/ultra-alien-fighter
- BreakOut: Joao Marco Barros, Leonardo Toledo- Repositorio: https://github.com/JoaoMOBarros/BreakOut
- Snake: Julio Ng, Caio Ostan - https://github.com/juliongusp/asm
- Snake Bomb: Raphael Medeiros, Matheus Sanchez, Sergio Piza - https://github.com/rmedeiros23/Snake-Bomb
- DinoFuga: Victor Momenté, Vinicius Cortizo, João Victor Alves - https://github.com/VictorMomente/AssemblyGame
- Em Buscar do corote perdido: Marco Antonio Gallo, Murilo Kazumi de Freitas, Rubens Galdino - https://github.com/rubensgaldinojr/assemblygame-corote
- Snake: Orlando Pasqual Filho - https://github.com/NirvanaZen/SSC0511-Organizacao-de-Computadores-Digitais
- Snake: Guilherme Queiroz/ Pedro Fazio - https://github.com/Guilherme292/Projeto-Snake-ORG-Comp-I
- DPJump: Leandro Giusti Mugnaini/Matheus Borges Kamla/Bruno Ribeiro Helou - https://github.com/leandromugnaini/jogoassembly
- Velha: Caio Alarcon, Denis Alexandre, Bruno Menzzano https://github.com/CaioAlarcon/projetos/tree/master/velha
- Snake Trifásico: Eduardo Zaboto Mirolli, Leonardo Bellini dos Reis, Vinícius Torres Dutra Maia da Costa - https://github.com/LeoBellini/jogo
- Donkey Kong: Leonardo Moreira Kobe, Matheus Bernardes dos Santos - https://github.com/lmkobe/DonkeyKongGame
- CampoMinado: Beatriz Guidi, Rafael Doering Soares: https://bitbucket.org/bguidi/jogoorgcompbeatrizrafael/src
- Invasão espacial: Gabriel Dos Santos Brito, Luiz Miguel Saraiva, Rafael Meliani Velloso - https://github.com/RafaelMV123/TRABALHO-DO-SIMOES
- Hoth: Henrique Freitas, Jayro Boy, Vinícius Finke - https://github.com/JayroBoy/OrgComp2018
- Snake Trifásico: Eduardo Zaboto Mirolli, Leonardo Bellini dos Reis, Vinícius Luiz da Silva Genésio - https://github.com/LeoBellini/jogo
- Bomberman: Gustavo Antonio Perin, João Vitor Monteiro, Marcelo Augusto Dos Reis - https://github.com/Bobagi/Bomberman
- Insira o seu projeto AQUI
- ICMC WARS: Gabriel Angelo Sgarbi Cocenza, Ingrid Hiromi Yagui Otani, Maria Vitória do N. Inocencio - https://github.com/gabrielcocenza/ICMC_WARS
-
-





Apresentação da Disciplina

Professor: Eduardo do Valle Simões
Email: Simoes AT icmc.usp.br
Departamento de Sistemas de Computação – ICMC - USP
Grupo de Sistemas Embarcados e Evolutivos
Laboratório de Computação Reconfigurável

Organização de Computadores Digitais
Créditos Aula: 	4
Créditos Trabalho: 	0

Objetivos
Introduzir os conceitos básicos de Lógica Digital e de organização de computadores.

Programa 
Arquitetura de processadores: elementos básicos, operação geral, macro instruções e microinstruções. Subsistemas de memória. Unidade de controle: fundamentos, desenvolvimento e implementação. Técnicas para organização de E/S, uso de DMA e Barramentos. Noções de Linguagens Montadoras.


Avaliação
Serão realizados 1 prova e um tabalho sobre os assuntos do programa.
1ª Prova: Dia xx/0x/2021 

REC: xx/0x/2021

A nota final será calculada pela média ponderada dessas notas obtidas pelo aluno no decorrer do semestre.
Norma de Recuperação:
(NP-2) / 5 * Mrec + 7 - NP, se Mrec > 5 Max { NP, Mrec }, se Mrec < 5

 
Bibliografia
Livro(s) Texto(s):
-TANENBAUM, A.S. Structured Computer Organization, Prentice Hall, 4th ed, 1999.
- STALLINGS, W. Arquitetura e Organização de Computadores, Prentice Hall, 5a. ed., 2002.
-PATTERSON, D.A.; HENNESSY, J.L. Computer Organization and Design: The Hardware/Software Interface, Morgan Kaufmann, 1994.
-MONTEIRO, M.A. Introdução à Organização de Computadores, 3a ed. Livros Técnicos e Científico Editora SA, 1996.
 
Programa da Disciplina:
A seguir, a ementa da disciplina é apresentada com uma descrição da maneira com que cada tópico será abordado e dos capítulos dos Livros (PATTERSON - 3rd ed, 2003 e MONTEIRO - 5a ed, 2007) que contém cada tópico:

1) Revisão de conceitos sobre blocos lógicos/básicos; subsistemas de memória, organização, síntese e análise;
- duas aulas iniciais de revisão de todas as estruturas necessárias para a disciplina: multiplexadores, registradores, memórias (construção, barramentos, operação), ULA, state-machine, barramentos e interconexões.
- Será dada uma lista de exercícios para testar o conhecimento dos alunos nestes pontos, levantar deficiências e corrigi-las o mais cedo possível.
- As aulas seguintes, apresentam o projeto incremental de um processador contendo ULA, Máquina de Controle, Memória, registradores e dispositivos de I/O, servindo também de revisão pratica de implementação destes sub-sistemas e suas interconexões.
- PATTERSON Cap. 1.1 a 1.3, 3.1 a 3.5 (3rd ed, 2003)
- MONTEIRO Cap. 1.1.3, 3.3.1.2, 3.4.1, 4.2.1, 4.2.3, 4.3.3, 6.2.1, 6.2.2, 6.3, 6.4, 6.5., E.1 (5a ed, 2007)

2) Arquitetura de processadores, elementos básicos, operação geral, macro instruções e microinstruções, unidade de controle, fundamentos, desenvolvimento e implementação.
- Serão vistas as arquiteturas de Harvard e Von Neumann e será feita uma breve caracterização de maquinas RISC e CISC e seus elementos básicos.
- Um processador genérico (baseado na arquitetura Von Neumann / RISC) será implementado progressivamente e serão dados diversos exemplos de operação de suas subestruturas para todo um conjunto completo de instruções (ex.: ADD, LOAD, STORE, CALL, JUMP, PUSH, PULL, SHIFT ...)
- Serão apresentadas unidade de controle implementadas por elementos lógicos e por microprograma. Será também apresentado progressivamente todo o algoritmo de uma unidade de controle capaz de controlar as operações do conjunto de instruções do processador implementado.
- Serão apresentadas as características de sistemas de memória.
- Estes pontos serão vistos em uma seqüência de varias aulas, nas quais o processador e a máquina de controle serão construídos para executar o conjunto de instruções, uma a uma, revendo então seus fundamentos, desenvolvimento e implementação.
- PATTERSON Cap. 2.1 a 2.8, 5.1 a 5.5, 5.7, 7.1 a 7.2 (3rd ed, 2003)
- MONTEIRO Cap.8.1 a 8.5, 11.1, 11.2, 11.3.1, 11.3.2, 11.3.3 (5a ed, 2007)

3) Técnicas para organização de E/S, uso de DMA. Barramentos, conceitos gerais, estudos de casos.
- Barramentos, E/S e DMA serão vistos em duas fases:
a) Durante a revisão e a implementação progressiva do processador, varias noções praticas de diversos tipos de barramentos (controle, endereço, dados) e de dispositivos de E/S serão apresentadas. Inclusive, será construída em aula uma interface de leitura de teclado e uma controladora de vídeo (bastante simplificada: Texto 20 linhas x 16 colunas), ilustrando um mapa de caracteres e uma memória de vídeo. Serão também apresentados os conceitos de portos de entrada e saída em microcontroladores e interrupções de E/S.
b) Serão apresentados em transparências vários exemplos de hierarquia e controle (arbitragem) de barramentos, E/S Programada e os conceitos, operação e exemplos de DMA. As transparências serão utilizadas para apresentar (e não resumir) as informações conceituais do Livro sobre estes temas.
- PATTERSON Cap. 8.1 a 8.7 (3rd ed, 2003)
- MONTEIRO Cap. 10.1 a 10.3, 10.5, D.3.1, D.3.2 (5a ed, 2007)

4) Noções de Linguagens Montadoras.
- Serão apresentados exemplos de compilação de linguagens de alto nível para instruções Assembly e a utilização de montadores para converter linguagem montadora em linguagem de máquina, apresentando exemplos simples de transformação de linguagem C em linguagem montadora, como, por exemplo, inicialização de variáveis ("Int A = 5;"), Loops (For), e condicional (Se-Entao).
- Também serão vistos vários exemplos de desenvolvimento de programas em linguagem Assembly realizando diversas funcionalidades inclusive E/S.
- PATTERSON Cap. 2.5 a 2.8, 2.10, 2.13, 2.15 (3rd ed, 2003)
- MONTEIRO Cap. C.2, C.3, C.6 (5a ed, 2007)


Metodologia:

- As aulas serão principalmente expositivas explicando-se na lousa os conteúdos do livro. Sendo assim, a cópia do material dado na lousa possibilitará a obtenção de uma síntese bem exemplificada do conteúdo do livro.
- Serão utilizadas transparências, apresentando os tópicos: arquitetura de processadores, elementos básicos, vários exemplos de hierarquia e controle (arbitragem) de barramentos, E/S Programada, configurações de memória e o conceito, operação e exemplos de DMA.
- Será disponibilizada uma ampla lista de exercícios sobre toda a matéria e marcada uma aula específica para tirar as duvidas dos alunos sobre os exercícios.
- Serão apresentadas em aula e disponibilizadas aos alunos ferramentas de CAD para a programação e simulação de processadores. Nestas ferramentas, serão apresentados a arquitetura do processador, seu mapa de memória, conjunto de instruções e programas em Assembly controlando dispositivos como sensores, motores e esteiras e alguns jogos.
- Será disponibilizada uma ferramenta que é um simulador de operação de um processador bastante semelhante ao que será apresentado e implementado em aula, desenvolvida por alunos veteranos do ICMC, incluindo os códigos (feitos em linguagem C) do algoritmo que simula os módulos do processador, o algoritmo da máquina de controle e um montador para os programas em linguagem de máquina. Com esta ferramenta, os alunos podem editar o processador, incluir novas instruções, reprogramar a máquina de controle para controlar estas novas instruções e editar o montador para montá-las para o código de maquina do processador.
- Serão também oferecidas aulas opcionais para a turma no laboratório, na qual serão apresentados a linguagem VHDL e a utilização das placas de FPGA recentemente adquiridas para a implementação de processadores próprios dos alunos. Serão fornecidas transparências com a síntese desta metodologia para os alunos.
- Alguns alunos serão convidados a apresentar implementações para seus colegas utilizando as ferramentas apresentadas para implementar software em linguagem montadora e controlar dispositivos com os seus microcontroladores, bem como os que implementarem processadores próprios em software com o simulador ou em hardware na placa de FPGA.
- Todo este material será disponibilizado diretamente aos alunos durante as aulas e através do github.


