# SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica
Disciplina SSC0713 - Sistemas Evolutivos e Aplicados à Robótica

- Professor: Eduardo do Valle Simões
- Email: simoes@@@@@@icmc.usp.br  (Obs.: só tem um @)
- Departamento de Sistemas de Computação – ICMC - USP
- Grupo de Sistemas Embarcados e Evolutivos
- Laboratório de Computação Reconfigurável

## Alunos de 2021 - Segundo semestre
- Lista de Presença - Favor assinar a Lista de Presença durante o horario das aulas - https://docs.google.com/spreadsheets/d/1sGX7MipL8YbiCJv6__YXyyJdH2JZc_o0XmHGGnFtzfo/edit?usp=sharing
- Você pode acompanhar sua Frequência aqui: https://docs.google.com/spreadsheets/d/1ZCAvXoh6DDmYQFocOg9qW7p7kzcjaoHTCBVk8GhdTGA/edit?usp=sharing
- As aulas começam dia 20/08/2021 e serão transmitidas pelo google Meet (sempre com o código https://meet.google.com/btu-crez-aro) no horário normal da disciplina: sexta 14:20-16:00h
- As aulas serâo gravadas e disponibilisadas nessa plataforma

## Avaliação
- A avaliação será por meio da apresentação de um trabalho - Implementação de um Algoritmo Evolutivo aplicado a um problema real
- Os alunos serão divididos em grupos de 2-3 alunos para implementação do trabalho prático para a avaliação 
- TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE!!

### Apresentação dos trabalhos Turma 2021 - Segundo semestre:
- Apresentação dos trabalhos pelo Google Meet será na SEMANA XX de Janeiro: marcar o dia escolhido para apresentacao na tabela do arquivo:  ==> LINK DOCS: https://docs.google.com/spreadsheets/d/1_ApKDWWo3zTJVUvQOwhrquvahLiqaPpVBGVFXWG8tdY/edit?usp=sharing

- Insira seu projeto informando: TÍTULO DO PROJETO - nomes dos alunos - NUMERO USP dos alunos - link pro github/gitlab 

- O github/gitlab deve conter o software do projeto que seria a aplicação de um Algoritmo Evolutivo a um problema real.
- Deve conter um Readme explicando o projeto e a instalação de eventuais bibliotecas... e o software deve estar muito bem comentado!!
- Tambem deve conter e um VÍDEO DE VOCË explicando o projeto (pode ser somente uma captura de tela...) - Upa o vídeo no youtube ou no drive e poe o link no Readme.
- Além do VÍDEO DE VOCË explicando o projeto, TODOS OS ALUNOS PRECISAM APRESENTAR O TRABALHO PESSOALMENTE por google Meet: a ser informado o link

## Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial

## Exemplos de Código dos Colegas:
- Como usar o Gnuplot dentro do seu programa - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- Como digitar funções matemáticas e o AG avaliar na hora - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Tutoriais

- Código de uma Rede neural MLP - https://github.com/ncalsolari/Rede-Neural-Generica-3-camadas-

# Aulas a Distância (2021 - Segundo Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Aula01 20/08/2021 (14:20h) - sempre com o código https://meet.google.com/btu-crez-aro




### Aulas a Distância (2021 - Primeiro Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Aula01 14/04/2021 (8:10h) - https://drive.google.com/file/d/1IFKu6XrHWcvWiLTZrapWj5pdPSdF50oD/view?usp=sharing

- Aula02 28/04/2021 (8:10h) - https://drive.google.com/file/d/1uT-mKPaEjSxJBWAfL44F-lV8ULvS1zvU/view?usp=sharing

- Aula03 05/05/2021 (8:10h) - https://drive.google.com/file/d/1o1s3h1nKTRNn60p6d5OyfNV_nACBz6fx/view?usp=sharing

- Aula04 12/05/2021 (8:10h) - https://drive.google.com/file/d/1W20L3KKPumAUEjWlVSuvSEPKQbbNVEOq/view?usp=sharing

- Aula05 19/05/2021 (8:10h) - https://drive.google.com/file/d/1F-lTk6_VOdSxqluFhs2kBClhBeeCGNYo/view?usp=sharing

- Aula06 26/05/2021 (8:10h) - https://drive.google.com/file/d/15SWIqClFiEcgk4V8IEQ5AOcvABM0-XJk/view?usp=sharing

- Aula07 02/06/2021 (8:10h) - https://drive.google.com/file/d/1GVp3h4KWVjKSzZrTqA9x90bVG8YuQOcv/view?usp=sharing

- Aula08 09/06/2021 (8:10h) - https://drive.google.com/file/d/1dBo4hDTdhBlt_tajueshaFHfZH2ZOt-r/view?usp=sharing

- Aula09 16/06/2021 (8:10h) - https://drive.google.com/file/d/1U4MJ6ihBfcvAR6kwwHDCrUbNbDbX0W6D/view?usp=sharing

- Aula10 23/06/2021 (8:10h) - https://drive.google.com/file/d/1Djsy3lgihpUbNkQfHO8OJ-NUj_Hx8YKj/view?usp=sharing

- Aula11 30/06/2021 (8:10h) - https://drive.google.com/file/d/1tkPenrqwi4fVwSMCtxQ_hLFsFTY4_BUl/view?usp=sharing

- Aula12 07/07/2021 (8:10h) - https://drive.google.com/file/d/1e8dDc40kJ9LQX8XsW9GSczgCCg3Q2gsR/view?usp=sharing

- Aula13 14/07/2021 (8:10h) - https://drive.google.com/file/d/1oG3XQvfRwIJfgEHdi5acl_AHt0hLjNJ0/view?usp=sharing

- Aula14 21/07/2021 (8:10h) - Nao gravamos o video, pois foram somente apresentacoes dos trabalhos - Link para o Google Meet - https://meet.google.com/hfs-orsn-cxv

### Aulas a Distância (2020 - Segundo Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Aula01 28/08/20 - https://drive.google.com/file/d/19JpdczqT4c1xcApiq8ilp4fUfZbaq5VR/view?usp=sharing

- Aula02 04/09/20 - https://drive.google.com/file/d/1oRDVC3Af1AzNIgl47478xd8JBT9IfpMP/view?usp=sharing
- Software Básico do Nathan - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan

- Aula03 11/09/20 - https://drive.google.com/file/d/1VnCS4sZ-frR_jq2PRo97PqU23AiSi4cw/view?usp=sharing

- Aula04 18/09/20 - https://drive.google.com/file/d/1NLOyzuGniccnklX3rbgVM3UNzVbnUGQt/view?usp=sharing 
- Vinicius Eduardo Araujo - Evolução de Compra de Cartas Magic - MTG-Price-Optmization-Buying - https://github.com/E3-7/MTG-Price-Optmization-Buying

- Aula05 25/09/20 - https://drive.google.com/file/d/1xhmP34Cwvs85fUbp1bb5tLCpRhLxGu0S/view?usp=sharing
- Radioactive Wolves Of Chernobyl (2014) - https://www.youtube.com/watch?v=2Op2ZIg4JxE
- Wolves of Chernobyl: The Myth (and Truth) (2020) Revealed - https://www.youtube.com/watch?v=O_I5Hr9AuHE

- Aula06 02/10/20 - **Nao haverá Aula devido a Semana da Computação**

- Aula06 08/10/20 - https://drive.google.com/file/d/1r6SmgK6MyV7JGYbTffNyHur53RqlkDUq/view?usp=sharing

- Aula07 16/10/20 - https://drive.google.com/file/d/1u0ECC3oO4sA_jQ5P7aYwJvICTSc47Hqo/view?usp=sharing
- Artigo Predação por Síntese - https://gitlab.com/simoesusp/disciplinas/-/blob/master/SSC0713-Sistemas-Evolutivos-Aplicados-a-Robotica/MaterialEmPDF/Predacao_por_Sintese.pdf

- Aula08 23/10/20 - https://drive.google.com/file/d/1DqtUlOmFXTE4EMYfmWz66V9xBzLzhJ2u/view?usp=sharing

- Aula09 06/11/20 - https://drive.google.com/file/d/1JS_kAFXwqNgGWLq18iUbAGXUXYre5b2d/view?usp=sharing
- Link para o video que assistimos da aula do semestre passado - Aula03 17/04/20 - https://drive.google.com/file/d/1DciqzaKlLS5Sfd60k5W_IeRQ-8UhlP-M/view?usp=sharing

- Aula10 13/11/20 - https://drive.google.com/file/d/1NJ7gAN6I3ypjDpmDF6OPvOaAjIJNsnsg/view?usp=sharing
- Tutorial Muito bom sobre algoritmo de Backpropagation of the Error - https://www.youtube.com/watch?v=Ilg3gGewQ5U

- Aula11 20/11/20 - https://drive.google.com/file/d/1WKze4teMuPO8VGDiBB31-FO1I3X-gFyR/view?usp=sharing

- Aula12 27/11/20 - https://drive.google.com/file/d/1ODlOe2_Cb6LiMgZqPzSNU4BR8U75RP3y/view?usp=sharing

- Aula13 04/12/20 - https://drive.google.com/file/d/1GPhYTWuOB3hH1XhjAKZR8hCgFis9kev6/view?usp=sharing

- Aula14 11/12/20 - https://drive.google.com/file/d/1bPlYMmPgqraI_nB4x904lCX4AEdp5bE3/view?usp=sharing

- Aula15 14/12/20 - Entre no google Meet (http://meet.google.com/) com o código **https://meet.google.com/hfs-orsn-cxv**       - Não usar mais o codigo **SSC0713**


## Aulas a Distância (2020 - Primeiro Semestre)

- O Material usado nas aulas está na pasta MaterialAulaDistancia

- Minicurso OpenGL do Breno - https://github.com/Brenocq/OpenGL-Tutorial
- A importância da biodiversidade quase invisível: formigas, as '"engenheiras do cerrado" - https://www.youtube.com/watch?v=DUWKhx0Wols

- Aula01 27/03/20 - https://drive.google.com/file/d/1m2jJbFNZUh4uCBkpVRRhe1ZICrkX4t9a/view?usp=sharing

- Aula02 03/04/20 - https://drive.google.com/file/d/1okiL76FewUbyGMTcjNSI0Xhy0rsizia2/view?usp=sharing

- Aula03 17/04/20 - https://drive.google.com/file/d/1DciqzaKlLS5Sfd60k5W_IeRQ-8UhlP-M/view?usp=sharing
 
- Aula04 24/04/20 - https://drive.google.com/file/d/1x0Gr7Thu5PmLKmeMsGQ2geE2ctAw67EB/view?usp=sharing

- Aula05 08/05/20 - https://drive.google.com/file/d/1fQeK7XeyXkqeBqEbSVvD7AMPlihQdV_I/view?usp=sharing
 
- Aula06 15/05/20 - https://drive.google.com/file/d/1wTGBSILAqvA8Ty9cPbfAdvF8lU9IM2bY/view?usp=sharing

- Aula07 22/05/20 - https://drive.google.com/file/d/141t541dhyBw7wmCqv-uZ-kmMtjAwcgv1/view?usp=sharing

- Aula08 29/05/20 - https://drive.google.com/file/d/1-9mafM0usfdlH-Ue5pdILSlU9jCwX4AI/view?usp=sharing

- Aula09 05/06/20 - https://drive.google.com/file/d/1Lyblp-0Ck3JQeHJSguRtt4_nxgfnQuZS/view?usp=sharing 

- Aula10 19/06/20 - https://drive.google.com/file/d/1Xmwua_vtdAHS998foTXc0awy0dSuwbiv/view?usp=sharing

- Aula11 26/06/20 - https://drive.google.com/file/d/1VOhy1vZ02Kfv-V_ENFubrqxKF0Ct8wDs/view?usp=sharing

A- Pareto efficiencyv- Conceitos Gerais  -  https://en.wikipedia.org/wiki/Pareto_efficiency

B- Otimização Multiobjetivo - https://ppgee.ufmg.br/defesas/1041M.PDF

C- MÉTODOS DE OTIMIZAÇÃO MULTIOBJETIVO - http://www.decom.ufop.br/prof/marcone/Disciplinas/POMineracao/arquivos/GuidoPantuza_Dissertacao.pdf

D- Métodos de Otimizacao Multiobjetivo  - https://www.teses.usp.br/teses/disponiveis/3/3143/tde-19112004-165342/publico/Kleber-Cap3.pdf

- Aula12 03/07/20 - https://drive.google.com/file/d/148tBOQcC8PabBglvoEpve2YVhnjtD88h/view?usp=sharing

- Aula13 10/07/20 - https://drive.google.com/file/d/1I_4F0oL3lWeZWrognvT3TnNaTAfGRPob/view?usp=sharing

- Aula14 17/07/20 - https://drive.google.com/file/d/11Q46_m-J-DuGai7VFKOPyld8qxZ38iVe/view?usp=sharing

- Aula15 24/07/20 - https://drive.google.com/file/d/1Zy3lSTjPzMxgLb4hxrCUuhSVXukycf5q/view?usp=sharing


## Lista de Projetos 2021 - Primeiro Semestre:

- Grupo 1 - Teris Evolutivo - Tiago Triques, Ricardo Araujo -  https://gitlab.com/tiagot/teris-evolutivo

- Grupo 2 - Type The Game - Matheus Ventura de Sousa, Luiz Fernando Rabelo - https://github.com/matheus-sousa007/TypeTheGameAssembly

- Grupo 3 - Genius Assembly  - Milena Corrêa da Silva, Guilherme Lourenço de Toledo, João Victor Sene Araujo - https://github.com/milenacsilva/genius-assembly

- Grupo 4 - Campo Minado (Assembly Minado) - Giovanni Shibaki Camargo, Lucas Keiti Anbo Mihara, Pedro Kenzo Muramatsu Carmo - https://github.com/giovanni-shibaki/SSC0119_Jogo_Assembler

- Grupo 5 - HanoiAsm - Marco Antônio Ribeiro de Toledo, Vitor Caetano Brustolin: https://github.com/Ocramoi/HanoiAsm

- Grupo 6 - Tomb of the Mask - Gabriel Alves Kuabara, Lourenço de Salles Roselino, Gabriel Victor Cardoso Fernandes: https://github.com/GKuabara/tomb-of-the-mask

- Grupo 7 - MemóriaAssembly - Pedro Henrique Borges Monici, Rodrigo Lopes Assaf, Diógenes Silva Pedro: https://github.com/pedromonici/MemoriaAssembly

- Grupo 8 - RISC Zero - Gabriel da Cunha Dertoni, Natan Henrique Sanches, João Francisco Caprioli Barbosa Camargo de Pinho: https://github.com/GabrielDertoni/risc_zero

- Grupo 9 - Forca 2 jogadores - Pedro Henrique Raymundi, Gabriel Fachini, Victor Lima: https://github.com/PedroRaymundi/Forca_2_jogadores_assembly

- Grupo 10 - Jogo da Velha - Matheus Henrique de Cerqueira Pinto, Gustavo Henrique Brunelli, Pedro Lucas de Moliner de Castro: https://github.com/CerqueiraMatheus/SSC0119-Projeto-Final

- Grupo 11 - Pong - Gabriel Vicente Rodrigues, João Pedro Gavassa Favoretti, Lucas Pilla Pimentel, Ciro Grossi Falsarella: https://github.com/gabriel-vr/PingPongAssemblyICMC 

- Grupo 12 - Sistema Operacional - Ariel de Oliveira Cardoso:
https://github.com/aocard/AOC_ICMC-OS

- Grupo 13 - Snake - Ana Cristina Silva de Oliveira, Gustavo de Oliveira Silva, Gustavo Lelli Guirao
https://github.com/momoyo-droid/snake-assembly

- Grupo 14 - Jogo da memória - Erick Patrick Andrade Barcelos, Vinicius Santos Monteiro, Yann Amado Nunes Costa
https://github.com/brcls/Jogo-da-Memoria-Assembly

- Grupo 15 - Breakout - Marcos Almeida, Beatriz Diniz, Tulio Santana Ramos, Victor Paulo Cruz Lutes: https://github.com/VictorLutes/BreakoutPraticaOrgComp 

- Grupo 16 - Tower Defense Digitação - Sofhia de Souza Gonçalves, Matheus Luis Oliveira da Silva e Guilherme Machado Rios: https://github.com/toschilt/IEEEOpenEvolutAlg

- Grupo 17 - Flappy Bird - Guilherme Ramos Costa Paixão, Filipe Augusto Oliveira da Costa e Marcus Vinícius Teixeira Huziwara: https://github.com/gp2112/FlappyBirdAssembly 

- Grupo 18 - Pega Bandeira - Érica Ribeiro Filgueira dos Santos: https://github.com/ericarfs/Pega-Bandeira-Assembly


### Lista de projetos 2020 - Segundo Semestre:

- Automatic adjustment of game parameters - Lucas Yuji Matubara, Vinícius Eduardo de Araújo, Pedro Guerra Lourenço, Marcus Vinicius Castelo Branco Martins - https://github.com/AraujoVE/OSRobotsGame
- Agar.io - Andre Santana Fernandes, Diogo Castanho Emídio, Leonardo Antonetti da Motta, Marcus Vinicius Santos Rodrigues, Olavo Morais Borges Pereira - https://github.com/AndreSFND/evolutive-agario - (RNA: https://github.com/l-a-motta/neural-network-base)
- Status de RPG Evolutivo - Luca Gomes, Vinicius Finke, Igor Lovatto, Rafael Velloso - https://gitlab.com/RafMV/status-de-rpg-evolutivo
- Poker Evolutivo - Thiago Henrique Vicentini, Rafael Kuhn Takano - https://github.com/ThiagoVicentini/PokerEvolutivo
- Evolutive Level Generation - Diany Pressato, Matheus da Silva Araujo, Michelle Wingter - https://github.com/M-A-rs/Evolutionary-Maze-Generation
- TSP(Travelling Salesman Problem) - Alberto Neves, Mathias Fernandes, Marcus Vinícius Medeiros Pará - https://github.com/Math-O5/Genetic-Travelling-Saleman-Problem
- Asteroids - Guilherme Hideki Tati, Leonardo Kobe, Joao Pedro Reis - https://github.com/guilhermeht/Asteroids_evolutionaryAlgorithms
- Simulating an Ecosystem - Paulo Henrique da Silva, Gustavo Tuani Mastrobuono - https://github.com/gumastro/ecosystem_simulation
- Genetic Shortest Path - Luiz Fernando S. E. Santos - https://github.com/LFRusso/genetic-shortest-path
- Seguidor de Linha Evolutivo - Joao Marco Barros, Matheus Borges Kamla  - https://github.com/MatheusBorgesKamla/Seguidor_de_Linha_Evolutivo 
- Wild Life Simulation - Henrique Tadashi Tarzia, Victor Akihito Kamada Tomita - https://github.com/Vakihito/Wild-Life-Simulation.git
- Jogo da Velha - Eduardo Souza Rocha - https://github.com/Edwolt/Jogo-da-Velha
- Basquete Evolutivo - Nelson Calsolari Neto - https://github.com/ncalsolari/BasqueteEvolutivo
- Shortest Path Genético - Pedro Henrique Nieuwenhoff - https://github.com/npdr/GeneticAlgorithm---Shortest-Path
- Reconhecimento Evolutivo de Strings - Nilo Conrado Messias Alves Cangerana, Gabriel Caurin Correa - https://github.com/nilocangerana/trab-algoritmos-evolutivos-2020
- Sistema Diferencial - Matheus Branco Borella, Natan Bernardi Cerdeira - https://github.com/DarkRyu550/Evo
- Root of a Function - André Luis Storino Junior - https://github.com/andrestorino/AG-Root-of-a-Function
- AGKnapsack - Gabriel Victor, João Marcos, Matheus Cunha - https://github.com/matheushw/AGKnapsack
- Máximo e mínimos de funções reais - Nathan Rodrigues de Oliveir - https://github.com/NathanTBP/SSC0713_Sistemas-Evolutivos-Aplicados-a-Robotica_Nathan/tree/master/Vfinal



### Lista de projetos 2020 - Primeiro Semestre:

- AGente Duplo - Guilherme Amaral Hiromoto, Paulo Matana da Rocha, Carlos Henrique Lima Melara - https://gitlab.com/charles2910/projetos-de-sistemas-evolutivos-ssc0713
- MoonlitEvolution: Matheus Tomieiro, Yago Poletto  - https://github.com/matheustomieiro/MoonlitEvolution
- Evolução com regiões: Gabriel Gama - https://github.com/GSoaresgama/simoes-regioes.git
- TDM-2D: Gabriel Gama - https://github.com/GSoaresgama/TDM-GA_2D
- Labyrinth Escape Evolution - Felipe de Oliveira, Matheus Tomieiro, Victor Vieira Custodio Reis, Yago Poletto - https://github.com/matheustomieiro/lesc-evolution
- N-Axis Robotic Arm Control - Breno Cunha Queiroz - https://github.com/Brenocq/N-AxisRoboticArmControl
- Honeybee Simulation - Breno Cunha Queiroz, Johnny Batista - https://github.com/Brenocq/Honeybee-Simulation
- Neuroevolutionary Investor - Gabriel de Oliveira Guedes Nogueira - https://github.com/Talendar/neuroevolutionary_investor
- Genetic Path Finding Individuals - Alyson Matheus Maruyama Nascimento - https://gitlab.com/alyson1907/genetic-path-finding-individuals
- Canhão Evolutivo - Giovanni Paolo Meloni - https://github.com/Giovanni-P-Meloni/SEAR_JogoGenetico
- Darwin Bot FC - Mateus Prado Santos - https://github.com/matprado/Darwin_Bot_FC
- No-Code GA - Gabriel Santos Souza - https://github.com/gsasouza/no-code-ga
- GAPuzzleGame - Vinicius Henrique Borges, Tiago José de Oliveira  Toledo Junior, Vinícius Nakasone Dilda - https://github.com/TNanukem/GAPuzzleGame
- Filas Evolutivas - Leonardo Alves Paiva, Rafael Pastre - https://github.com/rafael-pastre/SSC0713-Sistemas-Evolutivos 
- Genetic Algorithm for Epidemic Model parameter adjustment - Leonardo Alves Paiva, Rafael Pastre - https://github.com/rafael-pastre/SSC0713-Sistemas-Evolutivos-Epidemico 
- Player’s Best Stats - Rodrigo Mendes, Marcelo Moraes - https://github.com/MarceloMoraesJr/Sistemas-Evolutivos 
- MLP Genetic Tuning - Antonio Moreira, Matheus Alves, Luca Porto - https://github.com/Micanga/mlp_genetic_tuning
- Gerador de inimigos para jogos - Luana Terra, Paolo Scassa - https://github.com/LuTDC/sistemasevolutivos
- Tetris Modificado (work in progress) - Higor Tessari, Gabriel Alfonso Nascimento Salgueiro - https://github.com/Higor-Tz/Tetris
- Jogo da velha evolutivo - Arthur Amêndola Paschoal - https://github.com/Arthur-AP/Algoritmo-genetico-Jogo-da-Velha
- Random String - Alysson Oliveira - https://github.com/alysson-oliveira/Random-String
- Flies FindHome - Paulo Katsuyuki Muraishi Kamimura, Afonso Henrique Piacentini Garcia, Felipe Barbosa de Oliveira
https://github.com/mkatsuyuki/findhome
- Maximum of a Function - Guilherme Eiji Ichibara, Luccas Paroni e Thiago Benine 
https://github.com/thiagobenine/genetic-algorithm
- Smart Rockets - Diego da Silva Parra, Mateus Virginio Silva, Murilo Luz Stucki e Tainá Andrello Piai.
https://github.com/Virgini0/Evolutivos_SmartRockets


### Lista de projetos 2019:

- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem
- Teste Feliz - Tiago Triques https://gitlab.com/tiagotriques/banana
- A Saga do Herói - Clara Rosa Silveira, Lucas Fernandes Turci, Raphael Medeiros Vieira https://github.com/lucasturci/ProjetoEvolutivos
- Tom and Jerry - Tiago Marino Silva, Alexandre Galocha Pinto Junior, Eduardo Pirro - https://github.com/firewall1011/tom-and-jerry
- A Lenda de Baiacu - Gyovana Mayara Moriyama, Henrique Matarazo Camillo
https://github.com/HenriqueCamillo/A-Lenda-de-Baiacu/
- PathPlanning with Obstacles - Pedro Natali, Rafael Pinho e Carlos Franco - https://github.com/PedroNatali/Trabalho-Evolutivos
- Flappy Bird - Gustavo Verniano Angélico de Almeida, Caio Ferreira Bernardo - https://github.com/CaioFerreiraB/trab_evolutivos
- Tetris Evolution - Rafael Gongora Bariccatti - https://github.com/bariccattion/Tetris_Evolution
- Problema das N rainhas
https://github.com/felfipe/trabsimoes
- Coevolução: predador e presa - Alice Valença De Lorenci
https://github.com/AliceDeLorenci/pray-predator-coevolution
- Mountain Car - Caio Ostan, Leandro Giusti Mugnaini, Leonardo Vinicius de Oliveira Toledo
https://github.com/LeoToledo/MountainCarGenetico
- Super Gravitron AG Edition - Augusto Ribeiro Castro, Estevam Fernandes Arantes
https://github.com/Es7evam/Super-Gravitron-AG-Edition
- Houdini metaballs - Juliana Crivelli
https://github.com/jumc/houdini-genetic-optimization
- Problema das 8 Rainhas - Guilherme Brunassi Nogima, Leonardo Akel Daher
https://github.com/gbnogima/8queens
- Smart Rockets - Daniel Sivaldi Feres, Gabriel Scalici
https://github.com/GabrielScalici/Foguetes_Sistemas_Evolutivos
- Melhor caminho com obstáculos - Guilherme Blatt, Igor Rodrigues
https://github.com/guilherme-blatt/Caminho-AG
- Flappy Ball - Antonio Sebastian, Lucas Tavares
https://github.com/lucast98/GA---Flappy-Ball
- Evolutionary Line Follower - Helbert Moreira, Marcos Arce, Marianna Karenina, Vinícius Ribeiro
https://github.com/vinicius-r-silva/evolutionary-line-follower
- Dino Google - Óliver Savastano Becker, Gabriela Chavez, Rafael Farias Roque, Gabriel Eluan Calado
https://github.com/Oliver-Becker/ChromeDino_GeneticPlay
- Minigame Tiro ao Alvo - Gabriel Van Loon e Tamiris Tinelli
https://github.com/GabrielVanLoon/ag_minigame
- Pega-Pega Simulator - Rodrigo Bragato Piva
https://github.com/Rodrigo-P/Pega-pega



### Lista de projetos 2018:
- Genetic Max Value Calculator: André Fakhoury, Thiago Preischadt, Vitor Santana - https://andrefakhoury.github.io/genetic-max-value-calculator/
- Genetic clustering framework: Daniel Barretto, David Cairuz, João Guilherme Araújo, Luísa Moura - https://github.com/lusmoura/Darwin
- Neuroevolutional Car Racing game: Lucas Mitri, Lucas Marcondes, Luis Ricardo - https://github.com/lucasgdm/neuroevolution-car-racing 🏎️
- NEAT SuperHexagon + gambiarras do Windows: Leonardo Gomes, André Almada, Eduardo Misiuk, Ali Husseinat - https://github.com/leoagomes/super-intelligence
- Simon's Evolution (Tron tira a tampa!): Eleazar Braga, Fabrício Guedes, Gustavo Lopes - https://github.com/fabricio-gf/tira-a-tampa
- Circuit Optimizer: Guilherme Prearo, Gustavo Nicolau, Paulo Augusto, Vianna - https://github.com/gprearo/CircuitOptimizer
- Slither IO - Snake Evolution: Paulo Augusto, Guilherme Prearo, Gustavo Nicolau -https://github.com/Kotzly/Slither_IO_IA
- Elections in Brazil (Apresentacao em Video https://www.youtube.com/watch?v=eg5iIUG49T0) : Marilene Garcia, Maria Luisa do Nascimento - https://github.com/MariaLuisaNascimento/Trabalho-AlgEvo
- Image Aproximator: Bruno Bacelar Abe, Paula Cepollaro Diana - https://github.com/abe2602/EvolvingAlgs
- FlappyBird e Vinho: Andre Daher Benedetti, Marcelo Bertoldi Diani - https://github.com/MarceloBD/sistemasEvolutivos 
- Snake AI: Paulo Pinheiro Lemgruber Jeunon Sousa, Matheus Carvalho Nali - https://github.com/PauloLemgruberJeunon/AI_Snake
- Caixeiro Viajante (TSP): Murilo Baldi, Victor Roberti Camolesi - https://github.com/Murgalha/tsp-ga
- We Are The Robots (ainda incompleto): Atenágoras Souza Silva - https://gitlab.com/atenagoras/We_Are_The_Robots
- Simplex Evolutivo: Bruno Flávo, Edson Yudi, Rafael Amaro Rolfsen - https://github.com/Exilio016/AG-Solver
- Multithreaded Genetic Algorithm: Gabriel Romualdo Silveira Pupo - https://github.com/gabrielrspupo/multithread-ga
- Tic Tac Toe AI: Luís Eduardo Rozante de Freitas Pereira - https://github.com/LuisEduardoR/tic-tac-toe-genetic-algorithm
- Genetic Minimax Checkers/Racing Car Evolution: Gabriel Carvalho, Victor Souza Cezario - https://github.com/victorcezario97/GACheckers.git (Checkers) / https://github.com/GabrielBCarvalho/Evolutionary-Algorithm-Car (Racing Car)
- A.G. com redeus neurais (jogo da velha): Bráulio Bezerra, Samuel Ferreira, João Ramos, Victor Giovannoni - https://github.com/brauliousp/jogo_da_velha
- Vida de Inseto: Danilo Henrique Cordeiro, Gabriel Kanegae Souza, Marcos Vinicius Barros de Lima Andrade Junqueira - https://github.com/Dancorde/evolutionary-steering (link do trabalho rodando: https://dancorde.github.io/evolutionary-steering/)
- Sliding Puzzle AG: Gabriel Toschi de Oliveira, Ana Maia Baptistão, Fernanda Tostes Marana - https://github.com/gabtoschi/slidingpuzzleGA
- Ant Collony: David Souza Rodrigues, Marcos Wendell Souza de Oliveira Santos - https://github.com/MarcosWendell/Algoritmos_Evolutivos
- Tibia Neat: Alex Sander R. Silva - https://github.com/Psycho-Ray/Tibia-Neat
- FlappyBird: Bolinha Preta: Giovanni Attina do Nascimento, Paulo Andre Carneiro - https://github.com/PauloCarneiro99/Flappy-Bird
- Cinturão de Asteroides: Lucas Nobuyuki Takahashi, Marcelo Kiochi Hatanaka, Paulo Renato Campos Barbosa - https://github.com/pauloty/Cinturao-De-Asteroides
- FoguetesEvolutivos: Alexandre Norcia Medeiros, Giovani Decico Lucafo, Luiz Henrique Lourencao - https://github.com/alexandrenmedeiros/FoguetesEvolutivos
- Neurogen Snake: Guilherme Milan Santos - https://github.com/guilhermesantos/neurogen_snake
- lo e 248 AI, Miguel Gardini, João Pedro Mattos, Vitor Rossi Speranza - https://github.com/vrsperanza/2048-AI---Evolutive-algorithm
- Ploty , Vitor Santana Cordeiro, Thiago Preischadt Pinheiro, André Luís Mendes Fakhoury, - https://github.com/andrefak/genetic-algorithm-plotly
- ProblemaMaximizarItensCaminhao, Helen Santos Picoli - https://github.com/helenspicoli/AlgGenProject
- Space Invaders: Guilherme de Pinho Montemovo, Igor Barbosa Grécia Lúcio, Rafael Corradini da Cunha - https://github.com/rafaelcorradini/space-invaders-genetic-ia
- Black Jack, Guilherme dos Reis, Tiago Daneluzzi - https://github.com/daneluzzitiago/21_evolutivo
- Ecossistema com predador e presa: Ygor Pontelo, Marcelo Temoteo e Matheus Lucas - https://github.com/ygorpontelo/ecosystem


